Senior Linux est une distribution permettant aux séniors totalement débutants en informatique d'accéder facilement aux fonctions de base pour communiquer avec leurs proches, grâce à une interface simplifiée.

Elle inclut plusieurs outils et fonctions d'accessibilité tels que :

- un bureau avec de très grosses icônes limitant et facilitant l'accès aux fonctions essentielles ;
- accès à un menu plus classique pour les plus téméraires, mais personnalisé pour limiter les risques, grâce à un handymenu Senior ;
- épuration de la barre des tâches ;
- ouverture maximisée des fenêtres lorsque c'est possible, et nombre réduit de boutons de contrôles, de façon à tendre vers le monotâche ;
- thème personnalisé pour les fenêtres afin de rendre leurs différentes zones facilement perceptibles ;
- grande largeur pour les barres de défilement des fenêtres afin de les rendre plus facilement manipulables à la souris ;
- modification aisée de la taille du texte et du pointeur de la souris ;
- accès à une loupe ;
- vitesse de répétition des touches du clavier adaptée ;
- outils de synthèse vocale (lecteur d'écran, de page Web, de documents,...)
- outil de saisie de texte à la voix ;
- clavier virtuel ;
- épuration des menus des applications de mail et de navigation Web ;
- etc.

Au premier démarrage une fenêtre d'accueil propose d'ajouter des voix complémentaires pour la synthèse vocale, ainsi qu'un script permettant d'installer, pour les proches disposant des compétences suffisantes, d'outils pour la prise de contrôle à distance du PC depuis Internet. Cette fonction est intéressante pour le dépannage, ou tout simplement pour faire une démonstration d'utilisation à distance.

Cette distribution s'installe grâce à un méta paquet lancé par un script. Elle peut être installée sur des distributions de la famille Debian ou Ubuntu basées sur l'environnement graphique XFCE.

Testé sur :

- Debian XFCE Jessie et Stretch 32 et 64 bits
- DFLinux Jessie et Stretch
- Xubuntu 16.04 32 et 64 bits

Le présent Git contient les sources du projet ainsi que les paquets .deb.
Un dépôt a été créé pour permettre l'installation automatisée depuis Internet.

Le script d'installation peut être copié depuis le Git, ou téléchargé par

wget http://de-bric-et-de-broc.fr/senior-linux/installe-senior.sh

Il faut rendre ce script exécutable depuis le gestionnaire de fichiers, ou en ligne de commande par

chmod +x installe-senior.sh

Pour lancer le script depuis une distribution sur laquelle sudo est installé (Ubuntu, DFLinux), il suffit de faire

sudo ./installle-senior.sh

Depuis une Debian sur laquelle sudo n'est pas installé par défaut, il faut faire

su -c ./installe-senior.sh

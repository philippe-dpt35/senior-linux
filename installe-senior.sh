#!/bin/bash

# Fonction retournant le choix de l'utilisateur
choix () {
    reponse=" "
    question=$1
    defaut=$2
    until [[ ${reponse} =~ ^[OoNn]$ ]] || [ "$reponse" = "" ] ; do
        echo "$question (O/N) ? par défaut $2"
        read reponse
    done
    if [ -z "$reponse" ]
        then if [ "$defaut" = "oui" ]
                 then return 0
                 else return 1
             fi
             exit 0
    fi
    case "$reponse" in
        [Oo]) return 0 ;;
        [Nn]) return 1 ;;
    esac
}

# Vérifie l'environnement de bureau utilisé
xfce=$(ps -e | grep "xfce4-session")
#if [ "$XDG_CURRENT_DESKTOP" != "XFCE" ]
if [ -z "$xfce" ]
   then echo "
##################  ATTENTION #######################
Senior Linux nécessite l'environnement de bureau XFCE
pour fonctionner correctement. Or ce n'est pas 
l'environnement graphique actuellement utilisé.
Senior Linux pourra s'installer, mais ne fonctionnera
pas convenablement.
#####################################################
"
        if ! choix "Voulez-vous poursuivre l'installation malgré tout" "non"
           then exit 1
        fi
fi

# Ajoute le dépôt de Senior Linux
if ! [ -e "/etc/apt/sources.list.d/senior-linux.list" ]
   then echo "deb http://www.de-bric-et-de-broc.fr/senior-linux/ Senior-stable main" > /etc/apt/sources.list.d/senior-linux.list
        wget -O - http://www.de-bric-et-de-broc.fr/senior-linux/key/depot-senior.gpg.key | apt-key add -
fi

# Ajoute la section non-free du dépôt officiel si nécessaire
if lsb_release -d | grep "Debian"
    then if ! grep "^deb.*non-free" /etc/apt/sources.list
            then sed -i "s/[ \t]*$//g" /etc/apt/sources.list
                 for distrib in "jessie" "stretch"
                     do 
                        sed -i 's/^deb http.*'"$distrib"' main$/& contrib non-free/g' /etc/apt/sources.list
                        sed -i 's/^deb http.*'"$distrib"' main contrib$/& main contrib non-free/g' /etc/apt/sources.list
                 done 
         fi
fi

# Vérifie la présence d'Onboard dans les dépôts officiels
paquet_existant=$(apt-cache dumpavail | grep "Package: onboard")

if [ -z "$paquet_existant" ]
   then echo "
Le logiciel Onboard (clavier virtuel) n'est pas présent
dansles dépôts de le distribution que vous avez installée.
Ce logiciel n'est pas indispensable pour Senior Linux
mais l'entrée de menu proposant le clavier virtuel sera
inopérante.
"
        if ! choix "Poursuivre l'installation" "oui"
           then exit 1
        fi
fi

# Installe le méta-paquet Senior Linux
apt-get update
apt-get install senior-linux

exit 0

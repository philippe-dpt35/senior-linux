#!/usr/bin/python
# -*- coding: utf-8 -*-

#licence creatice commons CC BY-NC-SA 2.0 FR
#script modifie et adapte par Philippe Henaff de l ENPT 2.0 de de la société IMaugis  

import sys
from PyQt4 import QtGui,QtCore
from PyQt4.QtCore import *
import os,stat
from os.path import basename
import math
import subprocess
import commands
import shutil
#from shutil import copy

#from db1 import Ui_Dialog1--------------------------------------------------------------------------------------------------------------------------

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Dialog1(object):
    def setupUi(self, Dialog1):
        Dialog1.setObjectName(_fromUtf8("Dialog1"))
        Dialog1.setWindowModality(QtCore.Qt.ApplicationModal)
        Dialog1.resize(239, 470)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Dialog1.sizePolicy().hasHeightForWidth())
        Dialog1.setSizePolicy(sizePolicy)
        self.Wa = QtGui.QWidget(Dialog1)
        self.Wa.setGeometry(QtCore.QRect(10, 10, 221, 200))
        self.Wa.setAcceptDrops(False)
        self.Wa.setObjectName(_fromUtf8("Wa"))
        self.LElabel1 = QtGui.QLineEdit(Dialog1)
        self.LElabel1.setGeometry(QtCore.QRect(30, 280, 201, 27))
        self.LElabel1.setObjectName(_fromUtf8("LElabel1"))
        self.LElabel2 = QtGui.QLineEdit(Dialog1)
        self.LElabel2.setGeometry(QtCore.QRect(30, 310, 201, 27))
        self.LElabel2.setObjectName(_fromUtf8("LElabel2"))
        self.label = QtGui.QLabel(Dialog1)
        self.label.setGeometry(QtCore.QRect(10, 250, 131, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.PBicone = QtGui.QPushButton(Dialog1)
        self.PBicone.setGeometry(QtCore.QRect(130, 220, 100, 27))
        self.PBicone.setAutoDefault(True)
        self.PBicone.setObjectName(_fromUtf8("PBicone"))
        self.horizontalLayoutWidget = QtGui.QWidget(Dialog1)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(30, 430, 201, 41))
        self.horizontalLayoutWidget.setObjectName(_fromUtf8("horizontalLayoutWidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.PBannule = QtGui.QPushButton(self.horizontalLayoutWidget)
        self.PBannule.setDefault(True)
        self.PBannule.setObjectName(_fromUtf8("PBannule"))
        self.horizontalLayout.addWidget(self.PBannule)
        self.PBvalide = QtGui.QPushButton(self.horizontalLayoutWidget)
        self.PBvalide.setAutoDefault(True)
        self.PBvalide.setObjectName(_fromUtf8("PBvalide"))
        self.horizontalLayout.addWidget(self.PBvalide)
        self.label_2 = QtGui.QLabel(Dialog1)
        self.label_2.setGeometry(QtCore.QRect(110, 355, 121, 17))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.LEcom = QtGui.QLineEdit(Dialog1)
        self.LEcom.setGeometry(QtCore.QRect(30, 385, 201, 27))
        self.LEcom.setObjectName(_fromUtf8("LEcom"))
        self.PBcom = QtGui.QPushButton(Dialog1)
        self.PBcom.setGeometry(QtCore.QRect(10, 350, 100, 27))
        self.PBcom.setAutoDefault(True)
        self.PBcom.setObjectName(_fromUtf8("PBcom"))

        self.retranslateUi(Dialog1)
        QtCore.QMetaObject.connectSlotsByName(Dialog1)
        Dialog1.setTabOrder(self.LElabel1, self.LElabel2)
        Dialog1.setTabOrder(self.LElabel2, self.PBcom)
        Dialog1.setTabOrder(self.PBcom, self.LEcom)
        Dialog1.setTabOrder(self.LEcom, self.PBannule)
        Dialog1.setTabOrder(self.PBannule, self.PBvalide)
        Dialog1.setTabOrder(self.PBvalide, self.PBicone)

    def retranslateUi(self, Dialog1):
        Dialog1.setWindowTitle(QtGui.QApplication.translate("Dialog1", "Nouvelle Rubrique", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Dialog1", "Labels", None, QtGui.QApplication.UnicodeUTF8))
        self.PBicone.setText(QtGui.QApplication.translate("Dialog1", "Icone", None, QtGui.QApplication.UnicodeUTF8))
        self.PBannule.setText(QtGui.QApplication.translate("Dialog1", "Annuler", None, QtGui.QApplication.UnicodeUTF8))
        self.PBvalide.setText(QtGui.QApplication.translate("Dialog1", "Valider", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("Dialog1", " (optionnelle)", None, QtGui.QApplication.UnicodeUTF8))
        self.PBcom.setText(QtGui.QApplication.translate("Dialog1", "Commande", None, QtGui.QApplication.UnicodeUTF8))

#from db2 import Ui_Dialog2--------------------------------------------------------------------------------------------------------------------------
class Ui_Dialog2(object):
    def setupUi(self, Dialog2):
        Dialog2.setObjectName(_fromUtf8("Dialog2"))
        Dialog2.setWindowModality(QtCore.Qt.ApplicationModal)
        Dialog2.resize(444, 242)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Dialog2.sizePolicy().hasHeightForWidth())
        Dialog2.setSizePolicy(sizePolicy)
        self.Wa = QtGui.QWidget(Dialog2)
        self.Wa.setGeometry(QtCore.QRect(10, 10, 190, 190))
        self.Wa.setAcceptDrops(False)
        self.Wa.setObjectName(_fromUtf8("Wa"))
        self.LElabel1 = QtGui.QLineEdit(Dialog2)
        self.LElabel1.setGeometry(QtCore.QRect(230, 40, 201, 27))
        self.LElabel1.setObjectName(_fromUtf8("LElabel1"))
        self.LElabel2 = QtGui.QLineEdit(Dialog2)
        self.LElabel2.setGeometry(QtCore.QRect(230, 70, 201, 27))
        self.LElabel2.setObjectName(_fromUtf8("LElabel2"))
        self.label = QtGui.QLabel(Dialog2)
        self.label.setGeometry(QtCore.QRect(210, 10, 131, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.PBicone = QtGui.QPushButton(Dialog2)
        self.PBicone.setGeometry(QtCore.QRect(50, 200, 100, 27))
        self.PBicone.setAutoDefault(True)
        self.PBicone.setObjectName(_fromUtf8("PBicone"))
        self.horizontalLayoutWidget = QtGui.QWidget(Dialog2)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(230, 190, 201, 41))
        self.horizontalLayoutWidget.setObjectName(_fromUtf8("horizontalLayoutWidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.PBannule = QtGui.QPushButton(self.horizontalLayoutWidget)
        self.PBannule.setDefault(True)
        self.PBannule.setObjectName(_fromUtf8("PBannule"))
        self.horizontalLayout.addWidget(self.PBannule)
        self.PBvalide = QtGui.QPushButton(self.horizontalLayoutWidget)
        self.PBvalide.setAutoDefault(True)
        self.PBvalide.setObjectName(_fromUtf8("PBvalide"))
        self.horizontalLayout.addWidget(self.PBvalide)
        self.label_2 = QtGui.QLabel(Dialog2)
        self.label_2.setGeometry(QtCore.QRect(310, 115, 121, 17))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.LEcom = QtGui.QLineEdit(Dialog2)
        self.LEcom.setGeometry(QtCore.QRect(230, 145, 201, 27))
        self.LEcom.setObjectName(_fromUtf8("LEcom"))
        self.PBcom = QtGui.QPushButton(Dialog2)
        self.PBcom.setGeometry(QtCore.QRect(210, 110, 100, 27))
        self.PBcom.setAutoDefault(True)
        self.PBcom.setObjectName(_fromUtf8("PBcom"))

        self.retranslateUi(Dialog2)
        QtCore.QMetaObject.connectSlotsByName(Dialog2)
        Dialog2.setTabOrder(self.LElabel1, self.LElabel2)
        Dialog2.setTabOrder(self.LElabel2, self.PBcom)
        Dialog2.setTabOrder(self.PBcom, self.LEcom)
        Dialog2.setTabOrder(self.LEcom, self.PBannule)
        Dialog2.setTabOrder(self.PBannule, self.PBvalide)
        Dialog2.setTabOrder(self.PBvalide, self.PBicone)

    def retranslateUi(self, Dialog2):
        Dialog2.setWindowTitle(QtGui.QApplication.translate("Dialog2", "Nouvelle Commande", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Dialog2", "Labels", None, QtGui.QApplication.UnicodeUTF8))
        self.PBicone.setText(QtGui.QApplication.translate("Dialog2", "Icone", None, QtGui.QApplication.UnicodeUTF8))
        self.PBannule.setText(QtGui.QApplication.translate("Dialog2", "Annuler", None, QtGui.QApplication.UnicodeUTF8))
        self.PBvalide.setText(QtGui.QApplication.translate("Dialog2", "Valider", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("Dialog2", " (Obligatoire)", None, QtGui.QApplication.UnicodeUTF8))
        self.PBcom.setText(QtGui.QApplication.translate("Dialog2", "Commande", None, QtGui.QApplication.UnicodeUTF8))

#----------------------------------------------------------------------------------------------------------------------------------------------------

#nom_utilisateur= os.getlogin().upper()
nom_utilisateur= os.environ["LOGNAME"].upper()
message_accueil= "Bonjour "+nom_utilisateur+" !"
synthese_active = False

param= ['00', '01', '10', '11', '12', '20', '21', '22', '23', '26', '27', '29', '30', '31', '32', '33', '36', '37', '39']

d={29:[]}
rep_theme = rep_theme1 = theme = fond = ''
rep_config="/usr/share/BNE-Linux/params/"
rep_applis="/usr/share/applications/"
rep_temp_sauve=os.path.expanduser ("~/")

tb1=[]          # tableau des boutons de type 1
cwd=os.getcwd()
fondF2=None;
fenetre_princ=None
hauteur_fenetre=0
largeur_fenetre=0
fconfig={'accueil':'configAccueil.rc','niv1':'configNiveau1.rc','niv2':'configNiveau2.rc'}
intitule={'accueil':'accueil','niv1':'niveau 1','niv2':'niveau 2'}
modif=0
#dock_exist=0
processus = 'plank'
dia=None        # boite de dialogue de rubrique ou commande
diaout=None
pasclicdroit1=0  # pas de clic droit sur les icones B1 si != 0
pasclicdroit2=0  # pas de clic droit sur les icones B2 si != 0

Vide=1111

str8=lambda s:s

def lit(texte): # fait lire le texte par la synthèse vocale
    if os.path.isfile("/usr/share/mbrola/fr1/fr1"):
        subprocess.call(["espeak","-s150","-vmb/mb-fr1",unicode(texte,'utf-8')])        
    else:
        subprocess.call(["espeak","-vfr",unicode(texte,'utf-8')])

def int1(s):
    """Convertit chaine en entier."""
    try:
        ret = int(s)
    except ValueError:
        #Try float.
        ret = int(float(s))
    return ret

def float1(s):
    """Convertit chaine en float."""
    try:
        ret = float(s)
    except ValueError:
        ret = 0.0
    return ret

def conv_conf(un_statut):
    """ convertit le dictionnaire de config """
    global rep_theme,rep_theme1,theme,fond
    if un_statut=="admin":
        # taille de la fenêtre
        #d[0]=['800','600']
        #FenPrinc.sx=int1(d[0][0])
        #FenPrinc.sy=int1(d[0][1])
        ecran=QtGui.QApplication.desktop().screenGeometry()
        d[0]=[ecran.width(),ecran.height()]
        FenPrinc.sx=int1(d[0][0])
        FenPrinc.sy=int1(d[0][1])
    else :
# taille de la fenêtre
        if 0 in d :
          FenPrinc.sx=int1(d[0][0])
          FenPrinc.sy=int1(d[0][1])
# ratio d'affichage des icones niveau 1
    if 1 in d: FenPrinc.ratio = float1(d[1][0])
# répertoire de thème
    if 10 in d:
        rep_theme=str8(d[10][0])
        rep_theme1=rep_theme
# sous rép de thème
    if 11 in d:
        theme=str8(d[11][0])
        rep_theme += theme
# fond d'écran niveau 1
    if 12 in d: FenPrinc.fond=str8(d[12][0])
#---------------bouton 1-----------------------
# fond bouton non sélectionné
    if 20 in d: B1.nsel="QWidget {image: url("+(rep_theme if d[20][0]!='/' else '')+str8(d[20][0])+");}"
# fond bouton sélectionné
    if 21 in d: B1.sel ="QWidget {image: url("+(rep_theme if d[21][0]!='/' else '')+str8(d[21][0])+");}"
# reflet bouton et coord x,y
    if 22 in d:
        B1.reflet="QWidget {image: url("+(rep_theme if d[22][0]!='/' else '')+str8(d[22][0])+");}"
        B1.refletx=int1(d[22][1])
        B1.reflety=int1(d[22][2])
# taille bouton, l,h,espace entre les boutons, pos y de l'icone dans le bouton
    if 23 in d:
        B1.size_x=int1(d[23][0])
        B1.size_y=int1(d[23][1])
        B1.esp   =int1(d[23][2])
        B1.yicone=int1(d[23][3])
# taille et pos label 1 ligne  -> style police,x,y,l,h
    if 26 in d:
        B1.lstyle1=str8(d[26][0].strip("'\""))
        B1.l1x=int1(d[26][1])
        B1.l1y=int1(d[26][2])
        B1.l1l=int1(d[26][3])
        B1.l1h=int1(d[26][4])
# taille et pos label 2 lignes -> style police,x1,y1,x2,y2,l,h
    if 27 in d:
        B1.lstyle2=str8(d[27][0].strip("'\""))
        B1.l2x1=int1(d[27][1])
        B1.l2y1=int1(d[27][2])
        B1.l2x2=int1(d[27][3])
        B1.l2y2=int1(d[27][4])
        B1.l2l =int1(d[27][5])
        B1.l2h =int1(d[27][6])
#---------------bouton 2-----------------------
# fond bouton non sélectionné
    if 30 in d: B2.nsel="QWidget {image: url("+(rep_theme if d[30][0]!='/' else '')+str8(d[30][0])+");}"
# fond bouton sélectionné
    if 31 in d: B2.sel ="QWidget {image: url("+(rep_theme if d[31][0]!='/' else '')+str8(d[31][0])+");}"
# reflet bouton et coord x,y
    if 32 in d:
        B2.reflet="QWidget {image: url("+(rep_theme if d[32][0]!='/' else '')+str8(d[32][0])+");}"
        B2.refletx=int1(d[32][1])
        B2.reflety=int1(d[32][2])
# taille bouton, l,h,espace entre les boutons, pos y de l'icone dans le bouton
    if 33 in d:
        B2.size_x=int1(d[33][0])
        B2.size_y=int1(d[33][1])
        B2.esp   =int1(d[33][2])
        B2.yicone=int1(d[33][3])
# taille et pos label 1 ligne  -> style police,x,y,l,h
    if 36 in d:
        B2.lstyle1=str8(d[36][0].strip("'\""))
        B2.l1x=int1(d[36][1])
        B2.l1y=int1(d[36][2])
        B2.l1l=int1(d[36][3])
        B2.l1h=int1(d[36][4])
# taille et pos label 2 lignes -> style police,x1,y1,x2,y2,l,h
    if 37 in d:
        B2.lstyle2=str8(d[37][0].strip("'\""))
        B2.l2x1=int1(d[37][1])
        B2.l2y1=int1(d[37][2])
        B2.l2x2=int1(d[37][3])
        B2.l2y2=int1(d[37][4])
        B2.l2l =int1(d[37][5])
        B2.l2h =int1(d[37][6])
        
def litConfig(fconfig,un_statut):
    f=open(rep_config+fconfig,'r')
    while 1:
        l=f.readline()
        if l=='': break
        l=l.strip(' \n')
        n=l[0:2]
        if n not in param: continue
        c=l[3:].split(',')
        n=int(n)
        #if n==39:   d[29][-1].append(c)
        #elif n==29: d[29].append([]); d[29][-1].append(c);
        #else: d[n]=c
	if n==39:
	 #permet la lecture de la ligne de commande sans tenir compte des virgules, s'il y en a dedans
	 c=l[3:].split(',',4)
	 d[29][-1].append(c)
	else:
	 if n==29:
	  #permet la lecture de la ligne de commande sans tenir compte des virgules, s'il y en a dedans
	  c=l[3:].split(',',4)
	  d[29].append([]); d[29][-1].append(c)
	 else:
	  d[n]=c
    f.close()
    conv_conf(un_statut)
    
def ecrit(f,val,*l):
    f.write('{}:'.format(val))
    for a in l[0:-1]:
        f.write(a)
        f.write(',')
    f.write(l[-1])
    f.write('\n')

def sauve_config(chemin_source,chemin_destination):
    #global fconfig,interface,d,modif
    global d,modif
    #origine = rep_config+fconfig[interface]
    #destination = rep_temp_sauve+fconfig[interface]
    #ligne_commande="echo {} | sudo -S cp "+origine+" "+destination
    subprocess.call(['cp', chemin_source, chemin_destination])
    f=open(chemin_destination,'w')

    f.write('# 00: taille de la fenêtre l,h. si pas défini -> plein écran\n')
    #if FenPrinc.sx != 0 and 0 in d:
         #f.write('00:{},{}\n\n'.format(FenPrinc.sx, FenPrinc.sy))

    f.write("# 01: ratio d'affichage des icones niveau 1, automatique si pas défini\n")
    f.write('01:{}\n\n'.format(FenPrinc.ratio))

    f.write("# 10: répertoire thème\n")
    ecrit(f,'10',rep_theme1)

    f.write("\n# 11: thème dans le répertoire theme\n")
    ecrit(f,'11',theme)

    f.write("\n# 12: fond dans le répertoire theme\n")
    ecrit(f,'12',FenPrinc.fond)

    f.write("\n# 20: fond bouton 1 non sélectionné\n")
    ecrit(f,'20',d[20][0])

    f.write("\n# 21: fond bouton 1 sélectionné\n")
    ecrit(f,'21',d[21][0])

    f.write("\n# 22: reflet bouton 1,x,y\n")
    ecrit(f,'22',d[22][0], str(B1.refletx), str(B1.reflety))

    f.write("\n# 23: taille bouton 1  l,h,espace entre les boutons, pos y de l'icone dans le bouton\n")
    ecrit(f,'23',str(B1.size_x),str(B1.size_y),str(B1.esp),str(B1.yicone))

    f.write("\n# 26: taille et pos label 1 ligne  -> style police,x,y,l,h\n")
    ecrit(f,'26',B1.lstyle1,str(B1.l1x),str(B1.l1y),str(B1.l1l),str(B1.l1h))

    f.write("\n# 27: taille et pos label 2 lignes -> style police,x1,y1,x2,y2,l,h\n")
    ecrit(f,'27',B1.lstyle2,str(B1.l2x1),str(B1.l2y1),str(B1.l2x2),str(B1.l2y2),str(B1.l2l),str(B1.l2h))

    f.write("\n# 29: définition des boutons 1 -> logo, info-bulle,label1,label2\n"
    "#   39: définition des boutons 2 -> logo, info-bulle,label 1,label 2,commande\n")
    for b1 in tb1:
        b=b1.b1_[0]
        ecrit(f,'29',b[0],b[1],b[2],b[3],b[4] if len(b)>4 else '')
        for b2 in b1.b1_[1:]:
            ecrit(f,'  39',b2[0],b2[1],b2[2],b2[3],b2[4])

    f.write("\n# 30: fond bouton 2 non sélectionné\n")
    ecrit(f,'30',d[30][0])

    f.write("\n# 31: fond bouton 2 sélectionné\n")
    ecrit(f,'31',d[31][0])

    f.write("\n# 32: reflet bouton 2,x,y\n")
    ecrit(f,'32',d[32][0], str(B2.refletx), str(B2.reflety))

    f.write("\n# 33: taille bouton 2: largeur,hauteur,espace entre les boutons, pos y de l'icone dans le bouton\n")
    ecrit(f,'33',str(B2.size_x),str(B2.size_y),str(B2.esp),str(B2.yicone))

    f.write("\n# 36: taille et pos label 1 ligne  -> style police,x,y,l,h\n")
    ecrit(f,'36',B2.lstyle1,str(B2.l1x),str(B2.l1y),str(B2.l1l),str(B2.l1h))

    f.write("\n# 37: taille et pos label 2 lignes -> style police,x1,y1,x2,y2,l,h\n")
    ecrit(f,'37',B2.lstyle2,str(B2.l2x1),str(B2.l2y1),str(B2.l2x2),str(B2.l2y2),str(B2.l2l),str(B2.l2h))

    f.close
    modif=0
    fenetre_princ.setWindowTitle(FenPrinc.titre)


#-----------------------------------------------------------------------------------------------
class   B1(QtGui.QWidget):
    size_x=size_y=esp=yicone=refletx=reflety=0
    l1x=l1y=l1l=l1h=l2x1=l2y1=l2x2=l2y2=l2h=l2l=0
    sel=nsel=reflet=lstyle1=lstyle2=''
    pos=0   # position du bouton
    statut_utilisateur=''
    def __init__(self,parent,li=None,b1=None):
        super(B1,self).__init__(parent)
        self.hl=0
        
        if li:
            self.b1_=li   # on garde la liste des boutons 2 ainsi que le bouton 1 en local
        self.setFixedSize(B1.size_x,B1.size_y)      # widget support
        self.label1=self.label2=0
        if b1:              # b1 est le bouton à dupliquer
            self.creer(b1.b1_[0])
            self.bouge(b1.x,b1.y)
            self.highlight()
        else:               # création bouton premier niveau
            self.l=self.b1_[0]
            self.creer(self.l)
        if self.statut_utilisateur=="admin" and pasclicdroit1==0:
            self.setAcceptDrops(True)

# clic droit sur bouton B1
    def contextMenuEvent(self, event):     # QContextMenuEvent
        global fondF2 #,admin
        if not fondF2:  # si on n'a pas de sous-menu niveau 2
            menu=QtGui.QMenu("Niveau 1", self)
            if fenetre_princ.statut_utilisateur == "admin" and pasclicdroit1==0:
                modifAction = menu.addAction("Modification")
                suppAction =  menu.addAction("Suppression") if len(tb1)>1                   else Vide
                avantAction = menu.addAction("Avant")       if tb1.index(self)>0            else Vide
                apresAction = menu.addAction("Après")       if tb1.index(self)<(len(tb1)-1) else Vide
                menu.addSeparator()
	        sousmenu=menu.addMenu("Ajouter un bouton à la rubrique")
		rubriqueAction2=sousmenu.addAction("Choisir une application parmi celles installées")
	        rubriqueAction3=sousmenu.addAction("Un lien (internet, dossier partagé...)")
		rubriqueAction=sousmenu.addAction("Saisir soi-même les différents paramètres")
                action = menu.exec_(self.mapToGlobal(QPoint(event.x()+3,event.y()+3)))
#                    action = menu.exec_(self.mapToGlobal(event.pos()))
                if action == modifAction:
                    rubrique(self)
                if action == suppAction:
                    self.suppression()
                if action == avantAction:
                    self.avant()
                if action == apresAction:
                    self.apres()
                if action == rubriqueAction:
                    self.nouveauB2_manuel()
		if action == rubriqueAction2:
                    self.nouveauB2_auto()
		if action == rubriqueAction3:
                    self.nouveauB2_lien()

    def dragEnterEvent(self,event):
        
        if not self.hl:
            self.w.setStyleSheet(self.sel)
        event.acceptProposedAction()


    def dragLeaveEvent(self,event):
        
        if not self.hl:
            self.w.setStyleSheet(self.nsel)
        event.accept()

#    def dragMoveEvent(self,event):
#        event.acceptProposedAction()
    def choisir_appli(self):

	o = QtGui.QFileDialog.getOpenFileName(self, "Choisissez votre application", rep_applis, "Lanceurs applis (*.desktop)")
        if o:
            self.filename=o
            b2=B2(None,self.litDrop(self.filename))
	    b2=rubrique2(b2)
	    if b2:
                self.b1_.append(b2)

    def choisir_lien(self):
	lien, ok =QtGui.QInputDialog.getText(fenetre_princ,"Saisie du lien",'Saisissez ou copiez-collez le lien (internet, dossier partagé, ...) dans le champ de saisie ci-dessous')
	if (ok and not lien.isEmpty()):
	   nom_lien, ok =QtGui.QInputDialog.getText(fenetre_princ,"Saisie du nom du bouton",'Saisissez le nom à afficher')
	   if (ok and not nom_lien.isEmpty()):
        	r=["1/URL.png","","","",""]
		r[2]=nom_lien
		r[4]="xdg-open "+lien
		b2=B2(None,r)
	    	b2=rubrique2(b2)
	    	if b2:
                	self.b1_.append(b2)

    #------------------------------------------------------------------------		
    def dropEvent(self,event):
        mimeData = event.mimeData()
        #print mimeData
        if mimeData.hasUrls():
            urlList = mimeData.urls()
            imax=len(urlList)
            if imax>31: imax=31
            text=QString()
            for i in range(0,imax):
                url = urlList[i].path()
                text += url;
            b2=B2(None,self.litDrop(text))
            b2=rubrique2(b2)
            if b2:
                self.b1_.append(b2)
        event.acceptProposedAction()

    def testfile(self,name):
        file=QFile(name)
        # Si valid == true le fichier existe
        return file.exists()

    def name2file(self,name):
        n=QString()
        #print name
        if self.testfile(name): return name;
        n="/usr/share/icons/hicolor/scalable/apps/"+name+".svg"
        if self.testfile(n): return n
        n="/usr/share/"+name+"/icons/"+name+".svg"
        if self.testfile(n): return n
        n="/usr/share/app-install/icons/"+name+".svg"
        if self.testfile(n): return n
        n="/usr/share/icons/Humanity/apps/48/"+name+".svg"
        if self.testfile(n): return n
        n="/usr/share/icons/gnome/scalable/apps/"+name+".svg"
        if self.testfile(n): return n
        n="/usr/share/app-install/icons/"+name+".png"
        if self.testfile(n): return n
        n="/usr/share/"+name+"/icons/"+name+".png"
        if self.testfile(n): return n
        n="/usr/share/icons/hicolor/scalable/apps/"+name+".png"
        if self.testfile(n): return n
        n="/usr/share/icons/hicolor/64x64/apps/"+name+".png"
        if self.testfile(n): return n
        n="/usr/share/icons/hicolor/48x48/apps/"+name+".png"
        if self.testfile(n): return n
        n="/usr/share/icons/hicolor/3fenetre_princ2x32/apps/"+name+".png"
        if self.testfile(n): return n
        n="/usr/share/pixmaps/"+name #pour mes icones d'applis
        if self.testfile(n): return n
        n="/usr/share/icons/Faenza/apps/48/"+name+".png"
        if self.testfile(n): return n
        return ""

    def litDrop(self,nf):
        r=["1/question.png","","","",""]
        label=com=ico=""
        file=QFile(nf)
        regName=QRegExp("^Name(\\[fr(_FR)?\\])?=")
        regIcon=QRegExp("^Icon=")
        regcapIcon=QRegExp("Icon=(.*)")
        regExec=QRegExp("^Exec=")

        if not file.open(QIODevice.ReadOnly | QIODevice.Text):
            return r
        en=QTextStream(file)
        while  not en.atEnd():
            line = en.readLine();
            if line=="[Desktop Entry]":
                while not en.atEnd():
                    line = en.readLine()
                    if line.startsWith("["):
                        file.close()
                        return r
                    if line.contains(regName):
                        r[2]=line.split("=")[1]
                    elif line.contains(regIcon):
                        if regcapIcon.indexIn(line) > -1:
                            r[0]=self.name2file(regcapIcon.cap(1))
                    elif line.contains(regExec):
                        #com=line.split("=")[1]
			com=line[5:]
                        if "%" in com:
                            com=com.split("%")[0]
                            while com.endsWith(' '): com.chop(1)
                        r[4]=com
        file.close()
        return r        

    def nouveauB2_manuel(self):
        b=rubrique2()
        if b:
            self.b1_.append(b)
    def nouveauB2_auto(self):
        self.choisir_appli()

    def nouveauB2_lien(self):
        self.choisir_lien()

    def suppression(self):
        global modif
        ret = QtGui.QMessageBox.question(self, "Suppression", "Voulez-vous vraiment\nsupprimer ce bouton ?", QtGui.QMessageBox.Cancel, QtGui.QMessageBox.Yes)
        if ret == QtGui.QMessageBox.Yes:
            nb1=tb1.index(self)
            max=len(tb1)-1
            self.hide()
            self.setParent(None)
            if nb1<max:
                for i in range(max,nb1,-1):
                    tb1[i].bouge(*tb1[i-1].pos())
            del(tb1[nb1])
            distribue()
            modifie()

    def inverse(self,a,b):
        global modif
        # on bouge les icones
        posa=tb1[a].pos()
        posb=tb1[b].pos()
        tb1[a].bouge(*posb)
        tb1[b].bouge(*posa)
        tb1[a],tb1[b]=tb1[b],tb1[a]
        modifie()

    def apres(self):
        a=tb1.index(self)
        b=a+1
        self.inverse(a,b)

    def avant(self):
        a=tb1.index(self)
        b=a-1
        self.inverse(a,b)


    def creer(self,l):
#----widget de fond
        self.w=QtGui.QWidget(self)
        self.w.setFixedSize(B1.size_x,B1.size_y)
        self.w.setStyleSheet(self.nsel)
#----widget d'icone
        self.ico=QtGui.QWidget(self)
#        self.ico.setFixedSize(B1.size_x,B1.size_y)
        self.ico.setGeometry((B1.size_x-120)//2,self.yicone,120,B1.size_y)
        self.ico.setStyleSheet("QWidget {image: url("+(rep_theme if l[0][0]!='/' else '')+str8(l[0])+");}")
#        self.ico.move(0,self.yicone)
#----widget de reflet
        self.r=QtGui.QWidget(self)
        self.r.setFixedSize(B1.size_x,B1.size_y)
        self.r.setStyleSheet(self.reflet)
        self.r.move(self.refletx,self.reflety)
#----label
        if l[2]:       # si on a un label
#            self.label1=QtGui.QLabel(unicode(l[2],'utf-8'),self)
            self.label1=QtGui.QLabel(str8(l[2]),self)
#            self.label1=QtGui.QLabel(l[2],self)
            self.label1.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
            if l[3]:   # si on a un 2nd label
#                self.label2=QtGui.QLabel(unicode(l[3],'utf-8'),self)
                self.label2=QtGui.QLabel(str8(l[3]),self)
#                self.label2=QtGui.QLabel(l[3],self)
                self.label2.setStyleSheet(self.lstyle2)
                self.label1.setStyleSheet(self.lstyle2)
                self.label1.setFixedSize(self.l2l,self.l2h)
                self.label2.setFixedSize(self.l2l,self.l2h)
                self.label1.move(self.l2x1,self.l2y1)
                self.label2.move(self.l2x2,self.l2y2)
                self.label2.setAlignment(Qt.AlignHCenter)
            else:           # on n'a qu'un label
                self.label1.setStyleSheet(self.lstyle1.strip("'"))
                self.label1.setFixedSize(self.l1l,self.l1h)
                self.label1.move(self.l1x,self.l1y)

    def bouge(self,x,y):
        self.move(x,y)
        self.x=x; self.y=y

    def enterEvent (self, event):
        self.w.setStyleSheet(self.sel)
        if synthese_active :
            lit(self.b1_[0][2]+" "+self.b1_[0][3])

    def leaveEvent (self, event):
        if not self.hl:
            self.w.setStyleSheet(self.nsel)

    def mouseReleaseEvent (self, event):
        global fondF2
        if event.button()==Qt.LeftButton:
            if fondF2:
                fondF2.setParent(None)
                fondF2=None
            else:
                if len(self.b1_)>1:  # si on a au moins un bouton B2
                    fondF2=F2(fenetre_princ,self)
                else:
                    if len(self.b1_[0])>4 and self.b1_[0][4]:   # + de 4 args et commande pas vide
                        fenetre_princ.curseur_wait()
                        if self.b1_[0][4]=="niveau1" :
                            niv1config(self.statut_utilisateur)
                        else:
                            if self.b1_[0][4]=="niveau2" :
                              niv2config(self.statut_utilisateur)  
                            else :
                                if self.b1_[0][4]=="quitter" :
                                    resultat=saisie_MDP()
                                    if resultat==0 :
                                        #montrer_ou_cacher_tableau_de_bord("montrer")
                                        ouvrir_DOCK()
                                        sys.exit(app.exec_())
                                    else :
                                        message_information_mdp_refuse()
                                else:
                                    QProcess.startDetached(self.b1_[0][4])


    def highlight(self):
        self.w.setStyleSheet(self.sel)
        self.hl=1

    def pos(self):
        return (self.x,self.y)

#-----------------------------------------------------------------------------------------------
class   B2(QtGui.QWidget):
    size_x=size_y=esp=yicone=refletx=reflety=0
    l1x=l1y=l1l=l1h=l2x1=l2y1=l2x2=l2y2=l2h=l2l=0
    sel=nsel=reflet=lstyle1=lstyle2=''
    statut_utilisateur=''
    pos=0   # position du bouton
    def __init__(self,parent,li):
        super(B2,self).__init__(parent)
        if li: self.b2_=li   # on garde le bouton 2 en local
        self.setFixedSize(B2.size_x,B2.size_y)      # widget support
        self.label1=self.label2=0
        self.l=self.b2_[0]
#----widget de fond
        self.w=QtGui.QWidget(self)
        self.w.setFixedSize(B2.size_x,B2.size_y)
        self.w.setStyleSheet(self.nsel)
#----widget d'icone
        self.ico=QtGui.QWidget(self)
#        self.ico.setFixedSize(B2.size_x,B2.size_y)
        self.ico.setGeometry((B2.size_x-60)//2,self.yicone,60,B2.size_y)
        self.ico.setStyleSheet("QWidget {image: url("+(rep_theme if self.l[0]!='/' else '')+str8(self.l)+");}")
#----widget de reflet
        self.r=QtGui.QWidget(self)
        self.r.setFixedSize(B2.size_x,B2.size_y)
        self.r.setStyleSheet(self.reflet)
        self.r.move(self.refletx,self.reflety)
#----label
        if self.l[2]:       # si on a un label
            self.label1=QtGui.QLabel(self.b2_[2],self)
            self.label1.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
            if self.b2_[3]:   # si on a un 2nd label
                self.label2=QtGui.QLabel(self.b2_[3],self)
                self.label2.setStyleSheet(self.lstyle2)
                self.label1.setStyleSheet(self.lstyle2)
                self.label1.setFixedSize(self.l2l,self.l2h)
                self.label2.setFixedSize(self.l2l,self.l2h)
                self.label1.move(self.l2x1,self.l2y1)
                self.label2.move(self.l2x2,self.l2y2)
                self.label2.setAlignment(Qt.AlignHCenter)
            else:           # on n'a qu'un label
                self.label1.setStyleSheet(self.lstyle1.strip("'"))
                self.label1.setFixedSize(self.l1l,self.l1h)
                self.label1.move(self.l1x,self.l1y)

    def cache(self):
        self.hide()

    def bouge(self,x,y):
        self.move(x,y)
        self.x=x; self.y=y

    def pos(self):
        return (self.x,self.y)

    def enterEvent (self, event):
        self.w.setStyleSheet(self.sel)
        if synthese_active :
            lit(self.b2_[2]+" "+self.b2_[3])
            
    def leaveEvent (self, event):
        self.w.setStyleSheet(self.nsel)

    def mouseReleaseEvent (self, event):
        global synthese_active
        if event.button()==Qt.LeftButton and not dia:
            fenetre_princ.curseur_wait()
            if self.b2_[4]=="quitter" :
                if self.statut_utilisateur != "admin" :
                    resultat=saisie_MDP()
                    if resultat==0 :
                        #montrer_ou_cacher_tableau_de_bord("montrer")
                        ouvrir_DOCK()
                        sys.exit(app.exec_())
                    else :
                        message_information_mdp_refuse()
                else:
                    if modif :
                        reponse=QtGui.QMessageBox.warning(fenetre_princ, "QUITTER", "Vous allez quitter l'application\nalors que des modifications ont eu lieu.\nVoulez-vous continuer ?", QtGui.QMessageBox.No, QtGui.QMessageBox.Yes)
                        if reponse==QtGui.QMessageBox.Yes :
                            #montrer_ou_cacher_tableau_de_bord("montrer")
                            ouvrir_DOCK()
                            sys.exit(app.exec_())
                    else:
                        ouvrir_DOCK()
                        sys.exit(app.exec_())
            else :
                flashSplash(self)
                QProcess.startDetached(self.b2_[4],)
                if self.b2_[4] =="/usr/bin/orca" :
					# si Orca est lancé, on déclenche la lecture des boutons par la synthèse vocale
                    synthese_active = True
                fondF2.detruit()

# clic droit sur bouton B2
    def contextMenuEvent(self, event):     # QContextMenuEvent
        global pasclicdroit2
        if fenetre_princ.statut_utilisateur=="admin" and pasclicdroit2==0:
            menu=QtGui.QMenu("Niveau 2", self)
            modifAction = menu.addAction("Modification")
            suppAction = menu.addAction("Suppression")
            avantAction = menu.addAction("Avant") if F2.tb2.index(self)>0 else Vide
            apresAction = menu.addAction("Après") if F2.tb2.index(self)<len(F2.tb2)-1 else Vide
            action = menu.exec_(self.mapToGlobal(QPoint(event.x()+3,event.y()+3)))
            if action == modifAction:
                self.modification()
            if action == suppAction:
                self.suppression()
            if action == avantAction:
                self.avant()
            if action == apresAction:
                self.apres()
              

    def modification(self):
        b=rubrique2(self)
        if b:   # si on a un retour
            pos=self.pos()
            i=F2.tb2.index(self)
            self.setParent(None)
            self.hide()
            F2.tb2[i]=None
            F2.tb2[i]=B2(F2.fm2,b)
            F2.tb2[i].show()
            F2.tb2[i].bouge(*pos)
            F2.b1_.b1_[i+1]=b

    def inverse(self,a,b):
        global modif
        # on bouge les icones
        posa=F2.tb2[a].pos()
        posb=F2.tb2[b].pos()
        F2.tb2[a].bouge(*posb)
        F2.tb2[b].bouge(*posa)
        F2.tb2[a],F2.tb2[b]=F2.tb2[b],F2.tb2[a] # on inverse les boutons dans le menu niveau 2
        # on inverse aussi les boutons 2 stockés dans les boutons 1
        tb1[F2.nb1].b1_[a+1],tb1[F2.nb1].b1_[b+1]=tb1[F2.nb1].b1_[b+1],tb1[F2.nb1].b1_[a+1]
        modifie()

    def apres(self):
        a=F2.tb2.index(self)
        b=a+1
        self.inverse(a,b)

    def avant(self):
        a=F2.tb2.index(self)
        b=a-1
        self.inverse(a,b)

    def suppression(self):
        global modif
        ret = QtGui.QMessageBox.question(fenetre_princ, "Suppression", "Voulez-vous vraiment\nsupprimer ce bouton ?", QtGui.QMessageBox.Cancel, QtGui.QMessageBox.Yes)
        if ret == QtGui.QMessageBox.Yes:
            nb2=F2.tb2.index(self)
            max=len(F2.tb2)-1
            self.hide()
            self.setParent(None)
            if nb2<max:     # si ce n'est pas le dernier bouton
                for i in range(max,nb2,-1):
                    F2.tb2[i].bouge(*F2.tb2[i-1].pos())
            del(F2.tb2[nb2])
            del(tb1[F2.nb1].b1_[nb2+1])
            modifie()

#-----------------------------------------------------------------------------------------------
class F2(QtGui.QWidget):
    b1=0	# icone B1 highlight
    nb1=0   # numéro de bouton B1 dans tb1
    b2=0	# tableau de boutons niveau 2
    fm=0	# fond menu
    fm2=0	# fond menu transparent
    n2=0	# compteur de bouton l2
    tb2=[]  # liste des boutons B2
    b1_=0   # icone B1 d'origine
    statut_utilisateur=''
    #statut_utilisateur=''
    def __init__(self,parent,b1o):
        global dia,pasclicdroit1
        super(F2,self).__init__(parent)
        pasclicdroit1=1   # dans le cas de sous menu, on n'autorise pas le drag & drop sur les icones B1
        self.setGeometry ( 0, 0, FenPrinc.sx, FenPrinc.sy )
        self.fond=QtGui.QWidget(self)
        self.fond.setStyleSheet("border: none; image: none; background-color: rgba(0,0,0,160);")
        self.fond.setGeometry ( 0, 0, FenPrinc.sx, FenPrinc.sy )
        self.b1=B1(self,b1=b1o)
        posx,posy=b1o.pos()
        n2=len(b1o.b1_)-1
        F2.nb1=tb1.index(b1o)
        F2.b1_=b1o
        nbx=int(((FenPrinc.sx*4)/6-2*B1.esp)/(B2.size_x+B2.esp))	# nbre de boutons maxi en largeur
        nl=int(math.ceil(float(n2)/float(nbx)))                 # nb de lignes du menu
        nbx=math.ceil(float(n2)/float(nl))
        larg=int((nbx if n2>nbx else n2)*(B2.size_x+B2.esp)-B2.esp+2*B1.esp)	# largeur du sous menu
        if posx+B1.size_x > FenPrinc.sx/6+larg:
            departx=posx+B1.esp-larg
        else:
            departx=int(FenPrinc.sx/6)                           # x de départ du sous menu
        haut= int(2*B1.esp+(B2.size_y+B2.esp)*nl-B2.esp)  # hauteur du menu
        F2.fm=QtGui.QWidget(self);
        F2.fm.setGeometry ( departx, (posy-5-haut) if (posy+B1.size_y+5+haut)>=FenPrinc.sy else posy+B1.size_y+5, larg, haut)
        #F2.fm.setStyleSheet("image: none; background-color: rgba(134,204,85,255); border: 2px solid black; border-radius: 15px;")
        F2.fm.setStyleSheet("image: none; background-color: rgba(134,204,85,255); border: 2px solid black; border-radius: 15px;")
        F2.fm2=QtGui.QWidget(F2.fm)
        F2.fm2.setStyleSheet("background-color: none; border: none;")

        posx=B1.esp; posy=posx;
        self.i2=0
        F2.tb2=[]
        for b2a in b1o.b1_[1:]:     # on parcourt la liste des boutons 2
            F2.tb2.append(B2(F2.fm2,b2a))
	    #print b2a
            F2.tb2[-1].bouge(posx,posy)
            self.i2+=1
            if self.i2==nbx:
                posx=B1.esp; posy +=B2.size_y+B2.esp
                self.i2=0
            else: posx +=B2.esp+B2.size_x
        self.show()

    def mouseReleaseEvent (self, event):
        if event.button()==Qt.LeftButton:
            self.detruit()

# clic droit sur le fond
    def contextMenuEvent (self, event):
        pass    # on ne fait rien

    def detruit(self):
        global fondF2
        self.setParent(None)
        fondF2=None

    def __del__(self):
        global fondF2,pasclicdroit1
        self.b1=None
        self.hide()
        self.fond=None
        B2.tb2=[]
        fondF2=None
        pasclicdroit1=0

#-----------------------------------------------------------------------------------------------
class FenPrinc(QtGui.QMainWindow):
    sx = sy = 0
    ratio = 0
    fond=''
    titre=''
    statut_utilisateur=''
#    f=None
    def __init__(self):
        global tb1, modif
        super(FenPrinc,self).__init__()
        modif=0
        FenPrinc.titre="BNE-Linux"
        self.setWindowTitle(FenPrinc.titre)
        self.setStatusBar(None)
        if 0 not in d:
            
            #self.setWindowFlags(Qt.FramelessWindowHint)       # on enlève les décors
            #self.setWindowState(Qt.WindowFullScreen)          # on passe en plein écran
            self.setWindowState(Qt.WindowMaximized)          # on passe en plein écran
            #self.setStyleSheet("background:transparent;")     # pas de fond
            
            self.setAttribute(Qt.WA_X11NetWmWindowTypeDesktop)
            #self.setAttribute(Qt.WA_TranslucentBackground)    # transparent
            #setAttribute(QtCore.Qt.WA_NoSystemBackground)
            #self.setStyleSheet("QMainWindow {image: url("+(rep_theme if FenPrinc.fond[0]!='/' else '')+FenPrinc.fond+");}")
            
            #self.setWindowFlags(Qt.FramelessWindowHint)       # on enlève les décors
            #self.setWindowState(Qt.WindowFullScreen)          # on passe en plein écran
            
            self.labelAccueil=QtGui.QLabel(message_accueil,self)
            self.labelAccueil.setGeometry(10,10,500,30)
            self.labelAccueil.setStyleSheet("QLabel {font-size: 28px; color: white; font-weight: bold}");
            lit(message_accueil)
            self.setGeometry(QtGui.QApplication.desktop().screenGeometry())
            ecran=QtGui.QApplication.desktop().screenGeometry()
            FenPrinc.sx=ecran.width();		# largeur d'écran
            FenPrinc.sy=ecran.height();		# hauteur d'écran
            self.resize(FenPrinc.sx,FenPrinc.sy)
            self.move((ecran.width() - self.sx)/2, (ecran.height() - self.sy)/2) #mettre la fenetre au milieu de l'ecran
            self.setStyleSheet("QMainWindow {background-color: #00aa91;}")
            #FenPrinc.sx=800;		# largeur d'écran
            #FenPrinc.sy=600;		# hauteur d'écran
            
            #self.showFullScreen()
        else:
            #self.setFixedSize(self.sx,self.sy)
			#self.setFixedSize(800,600)
            self.setWindowFlags(Qt.FramelessWindowHint)       # on enlève les décors
            self.setGeometry(QtGui.QApplication.desktop().screenGeometry())
            ecran=QtGui.QApplication.desktop().screenGeometry()
            FenPrinc.sx=ecran.width();		# largeur d'écran
            FenPrinc.sy=ecran.height()-50;		# hauteur d'écran
            self.resize(FenPrinc.sx,FenPrinc.sy)
            self.move((ecran.width() - self.sx)/2, (ecran.height() - self.sy)/2) #mettre la fenetre au milieu de l'ecran
            #self.setStyleSheet("QMainWindow {image: url("+(rep_theme if FenPrinc.fond[0]!='/' else '')+FenPrinc.fond+");}")
            self.setStyleSheet("QMainWindow {background-color: #3b0663;}")
        for b1 in d[29]:
            tb1.append(B1(self,b1))
        distribue()

    def closeEvent(self, event):
          #Capture la fermeture de la fenêtre
          if modif :
              reponse=QtGui.QMessageBox.warning(fenetre_princ, "QUITTER", "Vous allez quitter l'application\nalors que des modifications ont eu lieu.\nVoulez-vous continuer ?", QtGui.QMessageBox.No, QtGui.QMessageBox.Yes)
              if reponse==QtGui.QMessageBox.Yes :
                  #montrer_ou_cacher_tableau_de_bord("montrer")
                  ouvrir_DOCK()
                  event.accept()
              else:
                  event.ignore()
          else:
              #montrer_ou_cacher_tableau_de_bord("montrer")
              ouvrir_DOCK()
              event.accept()
                    
                   
    def __del__(self):
        self.hide()

    def montre(self):
        self.show()

    def curseur_wait(self):
        self.setCursor(Qt.WaitCursor)
        QTimer.singleShot(3000, self.curseur_normal)

    def curseur_normal(self):
        self.setCursor(Qt.ArrowCursor)
        
# clic droit sur fond
    def contextMenuEvent (self, event):     # QContextMenuEveinterfacent
       if self.statut_utilisateur == "admin":
            menu=QtGui.QMenu(self)
            accueilAction = menu.addAction("Accueil")
            #niv1Action = menu.addAction("Niveau1")
            #niv2Action = menu.addAction("Niveau2")
            menu.addSeparator()
            saveAction = menu.addAction("Sauvegarder")
            rstAction = menu.addAction("Restaurer comme à l'origine")
            menu.addSeparator()
            importAction = menu.addAction("Importer une configuration")
            exportAction = menu.addAction("Exporter une configuration")
            menu.addSeparator()
            rubriqueAction=menu.addAction("Nouvelle rubrique")
            menu.addSeparator()
            eleveAction=menu.addAction("Quitter le mode modification")
            menu.addSeparator()
            demarrageAction=menu.addAction("Lancer l'application au démarrage de l'ordinateur")
#            action = menu.exec_(self.mapToGlobal(event.pos()))
            action = menu.exec_(self.mapToGlobal(QPoint(event.x()+3,event.y()+3)))
            if action==accueilAction:
                accueilconfig(self.statut_utilisateur)
            #elif action==niv1Action:
                #niv1config(self.statut_utilisateur)
            #elif action==niv2Action:
                #niv2config(self.statut_utilisateur)
            elif action==saveAction:
                if QtGui.QMessageBox.warning(self, "Sauvegarde", "Vous allez sauvegarder la configuration \""+intitule[interface]+"\"\nÊtes-vous d'accord ?", QtGui.QMessageBox.No, QtGui.QMessageBox.Yes)==QtGui.QMessageBox.Yes:
                        resultat=saisie_MDP_sauve_config()
                        if resultat == 0 :
                            QtGui.QMessageBox.information(fenetre_princ, "Sauvegarde","Sauvegarde réussie")
                        else:
                            message_information_mdp_refuse()
            elif action==rstAction:
                if QtGui.QMessageBox.warning(self, "Restauration", "Vous êtes sur le point de restaurer \nla configuration d'origine\""+intitule[interface]+"\"\nÊtes-vous d'accord ?", QtGui.QMessageBox.No, QtGui.QMessageBox.Yes)==QtGui.QMessageBox.Yes:
                    resultat=saisie_MDP_restaure_config()
                    if resultat == 0 :
                        restaure(self.statut_utilisateur)
                        QtGui.QMessageBox.information(fenetre_princ, "Restauration","Restauration réussie")
                    else:
                        message_information_mdp_refuse()
            elif action==rubriqueAction:
                rubrique()
            elif action==eleveAction:
                accueilconfig("eleve")
            elif action==demarrageAction:
                gestion_demarrage()
            elif action==importAction:
                importer_config(interface)
            elif action==exportAction:
                exporter_config(interface)
                                    
       else :
            menu=QtGui.QMenu(self)
            accueilAction = menu.addAction("Accueil")
            #niv1Action = menu.addAction("Niveau1")
            #niv2Action = menu.addAction("Niveau2")
            menu.addSeparator()
            modifAction = menu.addAction("Modifier")
            action = menu.exec_(self.mapToGlobal(QPoint(event.x()+3,event.y()+3)))
            if action==accueilAction:
                accueilconfig(self.statut_utilisateur)
            #elif action==niv1Action:
                #niv1config(self.statut_utilisateur)
            #elif action==niv2Action:
                #niv2config(self.statut_utilisateur)
            elif action==modifAction:
                resultat=saisie_MDP()
                if resultat == 0 :
                    self.statut_utilisateur="admin"
                    accueilconfig(self.statut_utilisateur)
                else:
                    message_information_mdp_refuse()

def rubrique(b1=None):  # si none->nouveau, si b1 -> modif
    global dia,diaout,tb1,fenetre_princ,pasclicdroit1
    pasclicdroit1=1   # pour évider le drop vers B1 dans les boites de dialogue
    dia=dial1(b1)
    dia.show()
    if dia.exec_():
        pasclicdroit1=0
        if b1:  # s'il s'agit d'une modif
            b=b1.b1_
            b[0]=diaout
            pos=b1.pos()
            b1.hide()
            b1.setParent(None)
            n=tb1.index(b1)
            tb1[n]=B1(fenetre_princ,b)
            tb1[n].bouge(*pos)
            tb1[n].show()
            distribue()
            dia=None
        else:   # il s'agit d'un ajout
            tb1.append(B1(fenetre_princ,[diaout]))
            tb1[-1].show()
            tb1[-1].setAcceptDrops(True)
            distribue()
            dia=None
        modifie()
    pasclicdroit1=0

def rubrique2(b2=None):  # si none->nouveau, si b2 -> modif
# la fonction retourne le B2 créé ou le B2 modifié
    global dia,diaout,pasclicdroit2
    pasclicdroit2=1
    dia=dial2(b2)
    dia.show()
    if dia.exec_():
        pasclicdroit2=0
        modifie()
        dia=None
        return diaout
    else:
        dia=None
        pasclicdroit2=0
        return None
    

def distribue():        # redistribue les icones niveau 1 sur le fond
    global d
    
    if 1 not in d:  # si ratio non défini
        FenPrinc.ratio=float(sx)/float(sy);
    nl=0; nb1=len(tb1)
    while nl<nb1:
        nl+=1
        if float((B1.size_x+B1.esp)*int(nb1/nl))/(nl*(B1.size_y+B1.esp)) > FenPrinc.ratio:
            continue;
        else: break;
    
    nx=int(math.ceil((float(nb1))/nl))                      # nbre de boutons en x
    px=int(FenPrinc.sx/2-((nx*(B1.size_x+B1.esp))-B1.esp)/2)    # coord de départ des boutons
    py=int(FenPrinc.sy/2-((nl*(B1.size_y+B1.esp))-B1.esp)/2)    # coord de départ des boutons
    n2=0; x1=px

    nb1=0
    for b1 in tb1:
        b1.bouge(x1,py)
        n2+=1
        if n2==nx:
            n2=0; x1=px; py+=B1.size_y+B1.esp
        else:
            x1 += B1.size_x+B1.esp

#-----------------------------------------------------------------------------------------------
class dial1(QtGui.QDialog):
    def __init__(self,b=None):
        global tb1, modif,diaout
        super(dial1,self).__init__()
        self.ui = Ui_Dialog1()
        self.ui.setupUi(self)
        if b:
            b1t=b.b1_[0:1]  # on ne prend que la def du bouton 1
            if len(b1t[0][3])==0: b1t[0][3]="--------" # si on n'a pas de label 2
            self.wa=B1(self.ui.Wa,b1t)  # création du bouton
        else:
            self.wa=B1(self.ui.Wa,[['1/question.png','','------','------']])
        self.ui.LElabel1.setText(self.wa.b1_[0][2])
        self.ui.LElabel2.setText(self.wa.b1_[0][3])
	#
	#
	#Permet de memoriser la ligne entière de l'item "command" d'un lanceur
        self.commande=self.wa.b1_[0][4] if len(self.wa.b1_[0])>4 else ""
	#
	#
        self.ui.LEcom.setText(self.commande)
        # centrage du bouton dans la zone prévue dans la boite de dialogue
        self.wa.bouge((self.ui.Wa.width()-B1.size_x)/2,(self.ui.Wa.height()-B1.size_y)/2)
        self.filename=self.wa.b1_[0][0]
        self.label1=self.wa.b1_[0][2]
        self.label2=self.wa.b1_[0][3]
        self.connect(self.ui.PBvalide, SIGNAL("clicked()"),self.valide)
        self.connect(self.ui.PBicone,  SIGNAL("clicked()"),self.icone)
        self.connect(self.ui.PBannule, SIGNAL("clicked()"),self.annule)
        self.connect(self.ui.PBcom,    SIGNAL("clicked()"),self.com)
        self.connect(self.ui.LElabel1, SIGNAL("textEdited(QString)"),self.texte1)
        self.connect(self.ui.LElabel2, SIGNAL("textEdited(QString)"),self.texte2)

    def valide(self):
        global diaout,dia


#        diaout=[self.filename,'',self.label1,self.label2]
        diaout=[self.filename,'',self.ui.LElabel1.text(),self.ui.LElabel2.text()]
        self.commande=self.ui.LEcom.text()
        if len(self.commande):
            diaout.append(self.commande)
        self.done(1)


    def com(self):
        o = QtGui.QFileDialog.getOpenFileName(self, "Sélection de commande", "~", "Fichiers Commande (*.*)")
        if o:
            self.commande=o
            self.ui.LEcom.setText(self.commande)

    def icone(self):
        o = QtGui.QFileDialog.getOpenFileName(self, "Sélection d'image", rep_theme, "Image Files (*.png *.jpg *.bmp)")
        if o:
            self.filename=o
            if self.filename.startsWith(rep_theme): # startsWith : fonction Qt
                self.filename=self.filename[len(rep_theme):]
            self.wa.ico.setStyleSheet("QWidget {image: url("+(rep_theme if self.filename[0]!='/' else '')+self.filename+");}")

    def annule(self):
        global diaout
        diaout=None
        self.done(0)

    def texte2(self):
        self.label2=self.ui.LElabel2.text()
        self.wa.label2.setText(self.label2)

    def texte1(self):
        self.label1=self.ui.LElabel1.text()
        self.wa.label1.setText(self.label1)

#-----------------------------------------------------------------------------------------------
class dial2(QtGui.QDialog):
    def __init__(self,b=None):  # li = liste de déf du bouton 2 et b= bouton 2 à éditer
        global diaout,dia 
        super(dial2,self).__init__()
        self.ui = Ui_Dialog2()
        self.ui.setupUi(self)
        if b:
            b2t=b.b2_  # b2 temporaire
            if len(b2t[3])==0:  # si on n'a pas de label 2
                b2t[3]="--------"
        else:
            b2t=['1/question.png','','------','------','------']
        self.wa=B2(self.ui.Wa,b2t)  # création du bouton
        # remplissage des champs
        self.ui.LElabel1.setText(self.wa.b2_[2])
        self.ui.LElabel2.setText(self.wa.b2_[3])
        self.commande=self.wa.b2_[4]
        self.ui.LEcom.setText(self.commande)
        # centrage du bouton dans la zone prévue dans la boite de dialogue
        self.wa.bouge((self.ui.Wa.width()-B2.size_x)/2,(self.ui.Wa.height()-B2.size_y)/2)
        self.filename=self.wa.b2_[0]
        self.label1=self.wa.b2_[2]
        self.label2=self.wa.b2_[3]
        self.connect(self.ui.PBvalide, SIGNAL("clicked()")          ,self.valide)
        self.connect(self.ui.PBicone,  SIGNAL("clicked()")          ,self.icone )
        self.connect(self.ui.PBannule, SIGNAL("clicked()")          ,self.annule)
        self.connect(self.ui.PBcom,    SIGNAL("clicked()")          ,self.com   )
        self.connect(self.ui.LElabel1, SIGNAL("textEdited(QString)"),self.texte1)
        self.connect(self.ui.LElabel2, SIGNAL("textEdited(QString)"),self.texte2)
	
    def valide(self):
        global diaout,dia
        if len(self.ui.LEcom.text()):   # si on a quelque chose dans commande
            diaout=[self.filename,'',self.ui.LElabel1.text(),self.ui.LElabel2.text(),self.ui.LEcom.text()]
            dia=None
            self.done(1)
        else:
            QtGui.QMessageBox.warning(self, "création de commande", "Vous devez entrer\nune commande", QtGui.QMessageBox.Close)
            self.ui.LEcom.setFocus()

    def com(self):
        o = QtGui.QFileDialog.getOpenFileName(self, "Sélection de commande", "~", "Fichiers Commande (*.*)")
        if o:
            self.commande=o
            self.ui.LEcom.setText(self.commande)

    def icone(self):
        o = QtGui.QFileDialog.getOpenFileName(self, "Sélection d'image", rep_theme, "Image Files (*.png *.jpg *.bmp)")
        if o:
            self.filename=o
            if self.filename.startsWith(rep_theme): # startsWith : fonction Qt
                self.filenainterfaceme=self.filename[len(rep_theme):]
            self.wa.ico.setStyleSheet("QWidget {image: url("+(rep_theme if self.filename[0]!='/' else '')+self.filename+");}")

    def annule(self):
        global diaout
        diaout=None
        self.done(0)

    def texte2(self):
        self.label2=self.ui.LElabel2.text()
        self.wa.label2.setText(self.label2)

    def texte1(self):
        self.label1=self.ui.LElabel1.text()
        self.wa.label1.setText(self.label1)

#-----------------------------------------------------------------------------------------------
def gestion_demarrage() :
    nom_fichier= "BNE-linux.desktop"
    origine = "/usr/share/BNE-Linux/"+nom_fichier
    destination=os.path.expanduser ("~/.config/autostart/"+nom_fichier)
    
    existe=os.path.exists(destination)
    reponse=QtGui.QMessageBox.warning(fenetre_princ, "Lancement au démarrage", "Voulez-vous lancer cette application au démarrage de l'ordinateur?", QtGui.QMessageBox.No, QtGui.QMessageBox.Yes)
    if reponse==QtGui.QMessageBox.Yes:
        if not existe:
            shutil.copyfile(origine, destination)
            os.system('chmod +x '+destination)
            #os.chmod(destination, stat.S_IXOTH)
            #os.chmod(destination, stat.S_IXUSR)
            #os.chmod(destination, stat.S_IEXEC)
            
    if reponse==QtGui.QMessageBox.No:
        if existe:
            os.remove(destination)
    QtGui.QMessageBox.information(fenetre_princ, "Gestion du démarrage","Opération effectuée.")
    
#--------------------------exporter_config(i)---------------------------------------------------------------------
def importer_config(i):
    if i=="accueil":
        nom_fichier_config="configAccueil.rc"
    if i=="niv1":
        nom_fichier_config="configNiveau1.rc"
    if i=="niv2":
        nom_fichier_config="configNiveau2.rc"

    
    QtGui.QMessageBox.information(fenetre_princ, "Importation","Vous devez importer le fichier de configuration nommé : "+'"'+nom_fichier_config+'"')
    la_source=choisir_fichier()
    if la_source !="" :
	nom_fichier_recupere=os.path.splitext(os.path.split(la_source)[1])[0]
        nom_fichier_recupere=nom_fichier_recupere+".rc"
        la_destination=rep_config+nom_fichier_recupere
        
        if nom_fichier_recupere==nom_fichier_config:
            resultat=saisie_MDP_import_export(la_source,la_destination)
            if resultat == 0 :
                QtGui.QMessageBox.information(fenetre_princ, "Importation","Importation réussie")
                config(i,"admin")
            else:
                message_information_mdp_refuse()
        else:
            QtGui.QMessageBox.information(fenetre_princ, "Importation","le nom du fichier n'est pas correct. Il faut trouver le fichier nommé : "+'"'+nom_fichier_config+'"' +".Opération abandonnée.")
    else:
        QtGui.QMessageBox.information(fenetre_princ, "Importation","Vous n'avez choisi aucun fichier. Opération abandonnée.")

def exporter_config(i):
    if i=="accueil":
        nom_fichier_config="configAccueil.rc"
    if i=="niv1":
        nom_fichier_config="configNiveau1.rc"
    if i=="niv2":
        nom_fichier_config="configNiveau2.rc"
    la_source=rep_config+nom_fichier_config
    QtGui.QMessageBox.information(fenetre_princ, "Exportation","Vous allez exporter le fichier de configuration nommé : "+'"'+nom_fichier_config+'"')
    Dossier_choisi=choisir_emplacement_exportation()
    if Dossier_choisi !="":
        la_destination=Dossier_choisi+"/"+nom_fichier_config
        shutil.copyfile(la_source, la_destination)
        QtGui.QMessageBox.information(fenetre_princ, "Exportation","Exportation réussie.")
    else:
        QtGui.QMessageBox.information(fenetre_princ, "Exportation","Vous n'avez choisi aucun dossier. Opération abandonnée.")

def choisir_emplacement_exportation():
    dirName = QtGui.QFileDialog.getExistingDirectory(fenetre_princ, 
           fenetre_princ.tr("Choisir le dossier de destination", 
           rep_temp_sauve+"/Bureau",));
    #pour convertir chemin en utf8 et ne plus avoir de pb accent avec Qstring
    dirName=unicode(dirName.toUtf8(), encoding="UTF-8")
    #------------------------------------------------------------------------
    return dirName


def choisir_fichier():
    fileName = QtGui.QFileDialog.getOpenFileName(fenetre_princ, 
           fenetre_princ.tr("Choisir le fichier de configuration"), 
           rep_temp_sauve+"/Bureau", 
           fenetre_princ.tr("Fichier de configuration (*.rc)"));
    #pour convertir chemin en utf8 et ne plus avoir de pb accent avec Qstring
    fileName=unicode(fileName.toUtf8(), encoding="UTF-8")
    #------------------------------------------------------------------------
    return fileName
#-----------------------------------------------------------------------------------------------

def validation_MDP_import_export(un_mot_de_passe,la_source,la_destination): 
#si mot de passe valide, renvoie 0
    ligne_commande="echo {} | sudo -S cp "+la_source+" "+la_destination
    ligne_commande=str(ligne_commande)
    status, output =commands.getstatusoutput(ligne_commande.format(un_mot_de_passe))
    return status #si status 0 mot de passe admin valide, sinon faux
    print status
    
def saisie_MDP_import_export(la_source,la_destination): #retourne  0 si le mot de passe est ok, sinon une valeur differente
	mdp, ok =QtGui.QInputDialog.getText(fenetre_princ,"Mot de passe",'Saisissez le mot de passe administrateur'+'\n'+u'(en général, celui de votre session).',QtGui.QLineEdit.Password)
	if (ok and not mdp.isEmpty()):
		validation=validation_MDP_import_export(mdp,la_source,la_destination)
	else :
		validation="-1"
	return validation
#-----------------------------------------------------------------------------------------------    
def process_dock_existe():
    global processus
    global dock_exist
    dock_exist=0
    s = os.popen('ps ax').read()
    #print s
    if processus in s:
        dock_exist=1
    #print dock_exist
    
def fermer_DOCK():
    #global dock_exist
    #if dock_exist == 1:
        subprocess.call(['killall', processus])
        #dock_exist=1
    #print dock_exist
    #print processus
        
def ouvrir_DOCK():
    #global dock_exist
    #if dock_exist == 1 :
        #lancer une application externe indépendante du programme qui la lance (ici, independante de BNE-Linux)
        pid=os.spawnlp(os.P_NOWAIT, "/usr/bin/"+processus, processus, "")
    #print dock_exist
    #print processus
   
def montrer_ou_cacher_tableau_de_bord(un_statut): #appel a la fonction : statut="montrer" ou "cacher"
    ligne_cacher='<property name="autohide" type="bool" value="true"/>'
    ligne_montrer='<property name="autohide" type="bool" value="false"/>'
    chemin_fichier_params=os.path.expanduser ("~/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml")
    
    f = file(chemin_fichier_params,"r")    # ouvrir le fichier
    chaine = f.read()                   # le charger dans une chaine de caracteres
    f.close()                           # fermer le fichier
    if un_statut=="montrer":
        result=chaine.replace(ligne_cacher, ligne_montrer)     # remplacer les donnees que tu souhaites -> chaine.replace(old, new)
    if un_statut=="cacher":
        result=chaine.replace(ligne_montrer, ligne_cacher) # remplacer les donnees que tu souhaites
    f = file(chemin_fichier_params,"w")    # ouvrir le fichier de sortie en ecriture  Tu peux ouvrir le meme si tu veux l'ecraser
    f.write(result)                     # ecrire le resultat dans le fichier
    f.close()          
    
#-----------------------------------------------------------------------------------------------
def validation_MDP(un_mot_de_passe): 
#copie un fichier temp du dossier ~/enpt/temp dans le dossier home pour valider le mdp administrateur
#si mot de passe valide, renvoie 0
    origine = rep_config+"temp/fichier_temp"
    destination=rep_config+"/temp/temp/"
    ligne_commande="echo {} | sudo -S cp "+origine+" "+destination
    status, output =commands.getstatusoutput(ligne_commande.format(un_mot_de_passe))
    return status 
#si status 0 mot de passe admin valide, sinon faux
    
def saisie_MDP(): #retourne  0 si le mot de passe est ok, sinon une valeur differente
	mdp, ok =QtGui.QInputDialog.getText(fenetre_princ,"Mot de passe",'Saisissez le mot de passe administrateur'+'\n'+u'(en général, celui de votre session).',QtGui.QLineEdit.Password)
	if (ok and not mdp.isEmpty()):
		validation=validation_MDP(mdp)
	else :
		validation="-1"
	return validation

#-----------------------------------------------------------------------------------------------

def validation_MDP_restaure_config(un_mot_de_passe): 
#gere la restauration de la config d'origine
#si mot de passe valide, renvoie 0
    source = rep_config+"sauve_config/"+fconfig[interface]
    destination=rep_config+fconfig[interface]
    
    ligne_commande="echo {} | sudo -S cp "+source+" "+destination
    status, output =commands.getstatusoutput(ligne_commande.format(un_mot_de_passe))
    le_statut=status
    return le_statut #si le statut egal 0 mot de passe admin valide, sinon faux
    
def saisie_MDP_restaure_config(): #retourne  0 si le mot de passe est ok, sinon une valeur differente
	mdp, ok =QtGui.QInputDialog.getText(fenetre_princ,"Mot de passe",'Saisissez le mot de passe administrateur'+'\n'+u'(en général, celui de votre session).',QtGui.QLineEdit.Password)
	if (ok and not mdp.isEmpty()):
		validation=validation_MDP_restaure_config(mdp)
	else :
		validation="-1"
	return validation

#-----------------------------------------------------------------------------------------------

def validation_MDP_sauve_config(un_mot_de_passe): 
#gere la restauration de la config d'origine
#si mot de passe valide, renvoie 0
    #origine = rep_config+"sauve_config/"+fconfig[interface]
    source= rep_temp_sauve+fconfig[interface]
    destination=rep_config+fconfig[interface]
    sauve_config(destination,source)
    ligne_commande="echo {} | sudo -S cp "+source+" "+destination
    status, output =commands.getstatusoutput(ligne_commande.format(un_mot_de_passe))
    subprocess.call(['rm', source])
    le_statut=status
    return le_statut #si le statut egal 0 mot de passe admin valide, sinon faux
    
def saisie_MDP_sauve_config(): #retourne  0 si le mot de passe est ok, sinon une valeur differente
	mdp, ok =QtGui.QInputDialog.getText(fenetre_princ,"Mot de passe",'Saisissez le mot de passe administrateur'+'\n'+u'(en général, celui de votre session).',QtGui.QLineEdit.Password)
	if (ok and not mdp.isEmpty()):
		validation=validation_MDP_sauve_config(mdp)
	else :
		validation="-1"
	return validation

#-----------------------------------------------------------------------------------------------

def message_information_mdp_refuse():
	QtGui.QMessageBox.information(fenetre_princ, "Opération annulée",
                "Vous n'avez pas écrit le bon mot de passe "
                "ou "
                "vous avez annulé sa saisie.")
#-----------------------------------------------------------------------------------------------
def flashSplash(self):
        # Be sure to keep a reference to the SplashScreen
        # otherwise it'll be garbage collected
        # That's why there is 'self.' in front of the name
        
        self.splash = QtGui.QSplashScreen(QtGui.QPixmap('/usr/share/BNE-Linux/SplashScreen.png'))

        # SplashScreen will be in the center of the screen by default.
        # You can move it to a certain place if you want.
        # self.splash.move(10,10)

        self.splash.show()

        # Close the SplashScreen after 2 secs (2000 ms)
        QtCore.QTimer.singleShot(5000, self.splash.close)

#-----------------------------------------------------------------------------------------------       
def restaure(un_statut):
    # print( rep_config+"sauve_config/"+fconfig[interface] , rep_config+fconfig[interface] )
    # copy( rep_config+"sauve_config/"+fconfig[interface] , rep_config+fconfig[interface] )
    #subprocess.call(['cp', rep_config+"sauve_config/"+fconfig[interface], rep_config+fconfig[interface]]) 
    config(interface,un_statut)
    

def modifie():
    global modif
    modif=1
    fenetre_princ.setWindowTitle(FenPrinc.titre+'*')

def config(i,un_statut):
    global fenetre_princ,tb1,d,interface,fconfig,modif
    if not modif or QtGui.QMessageBox.warning(fenetre_princ, "Changement d'interface", "Vous allez quitter l'interface \""+intitule[interface]+"\"\nsans avoir sauvegardé vos modifications.\nVoulez-vous continuer quand même ?", QtGui.QMessageBox.No, QtGui.QMessageBox.Yes)==QtGui.QMessageBox.Yes:
        tb1=[]      # on détruit les boutons B1
        d={29:[]}   # on détruit le dictionnaire de config des boutons B1 et B2
        fenetre_princ=None
        
        modif=0
        interface=i
        litConfig(fconfig[i],un_statut)
        B1.statut_utilisateur=un_statut
        B2.statut_utilisateur=un_statut
        F2.statut_utilisateur=un_statut
        fenetre_princ=FenPrinc()
        fenetre_princ.statut_utilisateur=un_statut
        fenetre_princ.montre()
        #process_dock_existe()
        fermer_DOCK()
        #montrer_ou_cacher_tableau_de_bord("cacher")
        

def accueilconfig(un_statut):
    config("accueil",un_statut)

def niv1config(un_statut):
    config("niv1",un_statut)

def niv2config(un_statut):
    config("niv2",un_statut)

def quel_groupe(le_statut):
    accueilconfig(le_statut)


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    # passage de qt en utf-8
    QTextCodec.setCodecForCStrings(QTextCodec.codecForName("UTF-8"))

    # passage dans la langue locale
    locale = QLocale.system().name().section('_', 0, 0);
    translator=QTranslator()
    translator.load("qt_" + locale, QLibraryInfo.location(QLibraryInfo.TranslationsPath))
    app.installTranslator(translator)

    quel_groupe("eleve")
    # exécution de l'interface
    #resultat=saisie_MDP()
    #validation=validation_MDP(le_mot_de_passe)
    
    sys.exit(app.exec_())



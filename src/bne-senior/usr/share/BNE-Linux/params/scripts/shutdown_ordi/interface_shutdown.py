# -*- coding: utf-8 -*-

"""
Module implementing MainWindow.
"""

from PyQt4.QtGui import QMainWindow
from PyQt4.QtCore import pyqtSignature
import commands,os, sys

from Ui_interface_shutdown import Ui_MainWindow

class MainWindow(QMainWindow, Ui_MainWindow):
 
    def __init__(self, parent = None):
        QMainWindow.__init__(self, parent)
        self.setupUi(self)
    
    @pyqtSignature("")
    def on_Non_clicked(self):
        sys.exit()
        raise NotImplementedError
    
    @pyqtSignature("")
    def on_Oui_clicked(self):
        os.system('dbus-send --system --print-reply --dest="org.freedesktop.ConsoleKit" /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Stop')
        raise NotImplementedError

#! /usr/bin/python
#-*- coding: utf-8 -*-

##############################################################################
# 
# Philippe Hénaff philippe.henaff@ac-rennes.fr - philippe.henaff@laposte.net
# Copyright (C) 2014 Philippe Hénaff
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

##############################################################################

from PyQt4 import QtGui

from PyQt4.QtCore import *
from interface_shutdown import MainWindow
import sys, os
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    #.........................................................................................................................
    #Permet de résoudre les problèmes d'accents dans les différents messages
    # passage de qt en utf-8
    QTextCodec.setCodecForCStrings(QTextCodec.codecForName("UTF-8"))
    # passage dans la langue locale
    locale = QLocale.system().name().section('_', 0, 0);
    translator=QTranslator()
    translator.load("qt_" + locale, QLibraryInfo.location(QLibraryInfo.TranslationsPath))
    app.installTranslator(translator)
    #.........................................................................................................................
    myapp = MainWindow()
    ecran=QtGui.QApplication.desktop().screenGeometry()
    myapp.move((ecran.width() - myapp.width())/2, (ecran.height() - myapp.height())/2) #mettre la fenetre au milieu de l'ecran
    myapp.show()
    sys.exit(app.exec_())

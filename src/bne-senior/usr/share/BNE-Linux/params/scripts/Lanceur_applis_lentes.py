#!/usr/bin/python
# -*- coding:utf-8 -*-
# -*- coding: iso-8859-1 -*-


import sys,os
from PyQt4 import QtGui, QtCore

class Dialog(QtGui.QDialog):
    def __init__(self, parent=None):
        super(Dialog, self).__init__(parent)
        
        self.setGeometry(QtGui.QApplication.desktop().screenGeometry())
        ecran=QtGui.QApplication.desktop().screenGeometry()
        #self.sx=ecran.width();		# largeur d'écran
        #self.sy=ecran.height();		# hauteur d'écran
        
        
        cmdargs = str(sys.argv) #recupere argument du script
        nom_logiciel=str(sys.argv[1])
        nom_usuel_logiciel=str(sys.argv[2])
      
        pid=os.spawnlp(os.P_NOWAIT, "/usr/bin/"+nom_logiciel, nom_logiciel, "")
        box=QtGui.QMessageBox.information(self, "Avertissement",
                u"Le logiciel "+'"'+nom_usuel_logiciel+'"'+u" a été lancé mais il est très long à démarrer. Il faudra être patient. Vous pouvez fermer cette fenêtre quand vous le désirez!")
        #box.move((ecran.width() - self.sx)/2, (ecran.height() - self.sy)/2)
        exit()


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)

    main = Dialog()
    
    main.show()

    sys.exit(app.exec_())
